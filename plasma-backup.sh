#!/bin/bash
# take a copy of the plasma configuration files in the ~/.backup/.config directory
# the default path is the $HOME of the current user but a different user can be indicated as first argument of the command line

plasma_backup(){
    for FILE in $(ls -l $HOME/.config/plasma* | grep '^-' | awk '{print $9}')
    do
        cp -u $FILE $HOME/.backup/.config
    done
    }

if [ $# -eq 1 ]
then
    HOME=/home/$1
fi

plasma_backup
