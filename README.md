**bug 427278** - Panel & desktop config of secondary monitor lost after ScreenConnectors renumbering
https://bugs.kde.org/show_bug.cgi?id=427278. The panel and desktop configruation of a monitor can get 'lost' after a ScreenConnectors renumbering, requiring manual editing of plasmashellrc to restore.


### Take a backup of your plasma settings
1. Backups are stored in the userś directory in a directory called .backup where a directory .config is also created to keep trace of the original path.

        $ mkdir .backup
        $ cd .backup
        $ mkdir .config

2. create a short bash script to make a copy of the plasma files

    ***plasma-backup.sh***

                #!/bin/bash
                # take a copy of the plasma configuration files in the ~/.backup/.config directory
                # the default path is the $HOME of the current user but a different user can be indicated as fir
                st argument of the command line

                plasma_backup(){
                    for FILE in $(ls -l $HOME/.config/plasma* | grep '^-' | awk '{print $9}')
                    do
                        cp -u $FILE $HOME/.backup/.config
                    done
                    }

                if [ $# -eq 1 ]
                then
                    HOME=/home/$1
                fi

                plasma_backup

3. make it executable and copy it into /usr/local/sbin

        $ chmod 755 plasma-backup.sh
        $ sudo cp plasma-backup.sh /usr/local/sbin

#### Whenever you logout, shutdown or restart
Add the backup command in  .bash_logout

       $  vim ~/.bash_logout

***~/.bash_logout***

                # ~/.bash_logout

                # backup the plasma layout configuration
                /usr/local/sbin/plasma-backup.sh

#### Whenever you go in suspend or hibernate mode
1. Create a systemd service to take a backup of the plasma settings

        $ vim plasma-backup.service

    ***/etc/systemd/system/plasma-backup.service***

                [Unit]
                Description=Backup the Plasma Desktop upon Disconnect, Suspend, Restart, Stop

                [Service]
                ExecStart=/usr/local/sbin/plasma-backup.sh <username>

                [Install]
                WantedBy=suspend.target hibernate.target

3. Enable and start the service:

        $ sudo cp plasma-backup.service /etc/systemd/system
        $ sudo systemctl daemon-reload
        $ sudo systemctl enable plasma-backup.service
        $ sudo systemctl start plasma-backup.service

4. Check if the service exists with systemd by using the command

        $ systemctl list-units | grep plasma
    or

        $ systemctl status plasma-backup.service

### Script to restore the backup

        $ vim restore-plasma.sh

***restore-plasma***

                #!/bin/bash

                #kquitapp plasmashell
                killall plasmashell
                cp -f ~/.backup/.config/* ~/.config
                nohup plasmashell >/dev/null 2>&1 </dev/null &
